public class Main {
    public static void main(String[] args) {
        Library library = new Library();
        Book book1 = new Book("Le Petit Prince", "Antoine", 1943, "123456789");
        library.addBook(book1);
        Book book2 = new Book("Harry Potter", "J.K. Rowling", 2000, "123456789");
        library.addBook(book2);
        Book book3 = new Book("The Hobbit", "J.R.R. Tolkien", 1937, "123456789");
        library.addBook(book3);
        Book book4 = new Book("The Lord of the Rings", "J.R.R. Tolkien", 1955, "123456789");
        library.addBook(book4);
        Book book5 = new Book("The original", "J.R.R. Tolkien", 1950, "123456789");
        library.addBook(book5);
        System.out.println(book1.getBookInfo());
        System.out.println(library.getBooksPublishedAfterYear(1950));
        System.out.println(library.getAuthorsOfBooksPublishedBeforeYear(1999));
        library.removeBook(book1);
        library.removeBook(book2);
        System.out.println(library.getBooksPublishedAfterYear(1950));
        System.out.println(library.getAuthorsOfBooksPublishedBeforeYear(1999));
    }
}
