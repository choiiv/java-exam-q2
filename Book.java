public class Book {
    String title;
    String author;
    int publicationYear;
    String ISBN;

    public Book(String title, String author, int publicationYear, String ISBN) {
        this.title = title;
        this.author = author;
        this.publicationYear = publicationYear;
        this.ISBN = ISBN;
    }

    public String getBookInfo() {
        return "Title: " + title + "Author: " + author + "Year: " + publicationYear + "ISBN: " + ISBN;
    }

    public String getAuthor() {
        return author;
    }

    public int getPublicationYear() {
        return publicationYear;
    }
}
