import java.util.*;
import java.util.stream.Collectors; 

public class Library {
    ArrayList<Book> books = new ArrayList<>();
    public void addBook(Book book) {
        books.add(book);
    }
    public void removeBook(Book book) {
        books.remove(book);
    }
    public ArrayList<Book> getBooksPublishedAfterYear(int year) {
        return books.stream().filter(book -> book.getPublicationYear() > year).collect(Collectors.toCollection(ArrayList::new));
    }

    public ArrayList<String> getAuthorsOfBooksPublishedBeforeYear(int year) {
        return books.stream().filter(book -> book.getPublicationYear() < year).map(book -> book.getAuthor()).distinct().collect(Collectors.toCollection(ArrayList::new));
    }
}
